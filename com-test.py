#!/usr/bin/env python3
# @License: MIT 2022 d-_-b
# @Links: https://gitlab.com/profhound/kom-test
from __future__ import print_function
from subprocess import Popen, PIPE, STDOUT # only nix like atm
import sys
import platform
import traceback
import datetime
import json
import time
import configparser

__version__ = '0.0.1.220226'

config = configparser.ConfigParser()
config.read('config.ini')

ping_ips = config.get('config', 'ping_ips', fallback='')
if ping_ips != '':
    ping_ips = ping_ips.split(' ')
openssl_connect_hosts = config.get('config', 'openssl_connect_hosts', fallback='')
if openssl_connect_hosts != '':
    openssl_connect_hosts = openssl_connect_hosts.split(' ')
output_file = config.get('config', 'output_file')
ping_count = config.getint('config', 'ping_count')
max_avg_ping = config.getint('config', 'max_avg_ping') # ms
speedtest_on_max_avg_ping = config.getboolean('config', 'speedtest_on_max_avg_ping')
min_speedtest_download = config.getint('config', 'min_speedtest_download') # bit/s 
min_speedtest_upload = config.getint('config', 'min_speedtest_upload') # bit/s 
speedtest_every_count = config.getint('config', 'speedtest_every_count')
speedtest_enabled = config.getint('config', 'speedtest_enabled')
sleep_loop = config.getfloat('config', 'sleep_loop')
speedtest_cmd = config.get('config', 'speedtest_cmd')
openssl_connect_cmd = config.get('config', 'openssl_connect_cmd')

# PLATFORM related commands see if platform.system below
ping_cmd = """ping -q -n -c {c} {ip}"""
ping_heading = ['ip', 'min', 'avg', 'max', 'mdev']
ping_pipes_args = """ | tail -1 | awk -F'=' '{ print $2 }' | awk -F' ' '{ print $1 }' | awk -F'/' '{ printf $1 "," $2 "," $3 "," $4 "," }'"""

if len(sys.argv) >= 2:
    output_file = sys.argv[1] 
if len(sys.argv) >= 3:
    ping_ips = sys.argv[2:]
    
if 'CYGWIN' in platform.system() or 'Windows' in platform.system():
    ping_cmd = """ping -n {c} {ip}"""
    ping_heading = ['ip', 'min', 'avg', 'max']
    ping_pipes_args = """ | tail -1 | sed -e 's/ms//g' | awk -F' ' '{ printf $3 $6 $9 }'"""

def eprint(*args, **kwargs):
    'print to stderr'
    print(*args, file=sys.stderr, sep=',', **kwargs) # comment out if you want stderr quite
    pass

def oprint(*args, **kwargs):
    'print to eprint and output_file'
    eprint(*args, **kwargs)
    f = open(output_file, 'a')
    print(*args, file=f, sep=',', **kwargs)
    f.close()  

def run(cmd):
    'run a cmd command pipe stderr to stdout close fds return (stdout(utf-8), return_code)'
    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    output = p.communicate()[0].decode("utf-8")
    return_code = p.wait()
    return output, return_code

def win32_parse_ping(stdout):
    'parse dos ping stdout'
    arr = stdout.split('\r\n')[-2].replace('ms', '').replace(',', '=').split('=')
    output = ','.join((arr[1], arr[3], arr[5], ''))
    return output

def ping(ip, c = ping_count, ping_cmd = ping_cmd, pipes = ping_pipes_args):
    'ping ping_count and pipe to return comma str ping_heading'
    cmd = ping_cmd.format(ip=ip, c=c) + pipes
    if 'Windows' in platform.system():
        cmd = ping_cmd.format(ip=ip, c=c)
    output, code = run(cmd)
    if 'Windows' in platform.system():
        output = win32_parse_ping(output)
    return output

def speedtest():
    'execute speedtest-cli and return json dict'
    cmd = speedtest_cmd
    try:
        output, code = run(cmd)
        if code != 0:
            return {'success': False, 'output': output, 'code': code }
        speedtest_dict = json.loads(output)
        speedtest_dict['success'] = True
        return speedtest_dict
    except Exception as err:
        eprint(traceback.format_exc())
        return {'success': False, 'err': str(err), 'output': output, 'code': code }

def openssl_connect(host):
    'execute openssl_connect and return json dict'
    cmd = openssl_connect_cmd.format(host=host)
    try:
        output, code = run(cmd)
        if code != 0:
            return {'success': False, 'output': output, 'code': code }
        result_dict = {}
        result_dict['success'] = True
        return result_dict
    except Exception as err:
        eprint(traceback.format_exc())
        return {'success': False, 'err': str(err), 'output': output, 'code': code }

def counter_now(counter):
   'return counter, datetime_now, str'
   return (str(counter), str(datetime.datetime.now()) )

def test(counter):
    'ping all ips if avg ping of ip > max_avg, speedtest will only run on the first below avg'
    speedtest_executed = False
    for ip in ping_ips:
        ping_result = '{},{}'.format(ip, ping(ip))
        ping_results = ping_result.split(',')
        if len(ping_results) < 3:
            eprint('ERROR-PING', ping_result)
            continue
        try:
            ping_avg = float(ping_results[2])
        except Exception as err:
            eprint(err), eprint('ERROR', ping_result)
            ping_avg = max_avg_ping + 1 # force speedtest
        meta = counter_now(counter)
        if ping_avg > max_avg_ping:
            speedtest_dict = None
            if speedtest_on_max_avg_ping and speedtest_executed == False:
                speedtest_dict = speedtest()
                speedtest_executed = True
            oprint('status', 'counter', 'datetime' , *ping_heading , 'speedtest_json', '')
            oprint('WARNING-PING' , *meta , ping_result , json.dumps(speedtest_dict), '')
        else:
            eprint('ACCEPTABLE', *meta, ping_result)
    for host in openssl_connect_hosts:
        result_dict = openssl_connect(host)
        meta = counter_now(counter)
        if result_dict.get('success', False) == False:
            oprint('WARNING-SSL', *meta, host, json.dumps(result_dict), '')
        else:
            eprint('ACCEPTABLE', *meta, host, 'SSL exited successfully', '')

def main():
    'run ping test forever, only print WAR and ERR, speedtest every speedtest_every_count iterations'
    counter = 0
    oprint('STARTED', *counter_now(counter))
    eprint('printing output to', output_file)
    while True:
        test(counter)
        if speedtest_enabled and counter % speedtest_every_count == 0:
            speedtest_dict = speedtest()
            meta = counter_now(counter)
            if speedtest_dict.get('success', False) == False:
                oprint('status', 'counter', 'datetime', 'speedtest_json', '')
                oprint('ERROR-SPEEDTEST', *meta, json.dumps(speedtest_dict), '')
            elif speedtest_dict['download'] < min_speedtest_download or speedtest_dict['upload'] < min_speedtest_upload:
                oprint('status', 'counter', 'datetime', 'speedtest_json', '')
                oprint('WARNING-SLOW-SPEED', *meta, json.dumps(speedtest_dict), '')
            else:
                eprint('ACCEPTABLE', *meta, json.dumps(speedtest_dict), '')
        counter += 1
        time.sleep(sleep_loop)

if __name__ == '__main__':
    main()