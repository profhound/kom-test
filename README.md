## kom-toets.py
#### `MIT License 2022 d-_-b`

### desc

Repeatably ping ip's, test openssl and speedtest.net network connection for docker, debian, windows written in python3.

### config

Edit `vi config.ini` to change config options.

### csv status's

- `WARNING-PING`: Ping higher than `max_avg_ping`(default: 200ms).
- `ERROR-SPEEDTEST`: Could not com with speedtest.net see `speedtest_json` column for error (why so _many_ `<urlopen error _ssl.c:1106: The handshake operation timed out>`).
- `WARNING-SLOW-SPEED`: Speedtest upload or download below (default: 2.4 Mbps).
- `WARNING-SSL`: openssl s_client connect failed.

### run from `sh`

```sh
./com-test.py output.txt 1.1.1.1 8.8.8.8 # ... list of ips
```

### run in a container

```sh
docker-compose up
```

### windows

Download [com-test-win64.zip](https://gitlab.com/api/v4/projects/33994022/packages/generic/com-test/0.0.1/com-test-win64.zip) (12.9MB) unzip and execute `com-test.bat`.
